package hu.icell.hr.manager.web;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.icell.hr.manager.dao.EmployeeDAO;
import hu.icell.hr.manager.model.Employee;

@WebServlet(urlPatterns = { "/employees" })
public class EmployeeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7238797967265067623L;

	@Inject
	private EmployeeDAO dao;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<Employee> employees = dao.getAll();


		for (Employee e : employees) {
			resp.getWriter().println("Employee found: " + e);
		}
	}

}
